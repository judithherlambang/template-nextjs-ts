import type { LayoutProps } from "../types/PageWithLayouts"
import { MainNavbar } from "@/components/MainNavbar"

export const MainLayout: LayoutProps = ({ children }: any) => {
    return (
    <>
        <MainNavbar />

        {children}
    </>
    )
}