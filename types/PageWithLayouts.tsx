import { NextPage } from "next"
import type { ReactElement } from "react"
import { MainLayout } from "@/layouts/MainLayout"
import { AuthLayout } from "@/layouts/AuthLayout"

export type PageWithMainLayout = NextPage & { layout: typeof MainLayout }
export type PageWithAuthLayout = NextPage & { layout: typeof AuthLayout }

export type PageWithLayoutType =
 | PageWithMainLayout
 | PageWithAuthLayout

 export type LayoutProps = ({
    children,
    }: {
    children: ReactElement
    }) => ReactElement

export default PageWithLayoutType