import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { AuthLayout } from '@/layouts/AuthLayout'

Login.layout = AuthLayout

export default function Login() {
    return (
        <>

        <Head>
            <title>Login</title>
        </Head>

        <section id='login-page' className='flex items-center justify-center min-h-screen'>
            <div className="card bg-base-200 shadow-xl w-96">
                <div className="card-body">
                    <h2 className="card-title mx-auto mb-4">Login</h2>

                    <div className="mb-4">
                        <label className="form-control">
                            <div className="label">
                                <span className="label-text">Email</span>
                            </div>

                            <input type="email" placeholder="Type here" className="input input-bordered" name="email" />
                        </label>
                    </div>

                    <div className="mb-4">
                        <label className="form-control">
                            <div className="label">
                                <span className="label-text">Password</span>
                            </div>

                            <input type="password" placeholder="Type here" className="input input-bordered" name="password" />
                        </label>
                    </div>

                    <div className="card-actions mb-4">
                        <button className="btn btn-primary w-full" type='submit'>Login</button>
                        <Link href="/" className="btn btn-error w-full">Back</Link>
                    </div>

                    <span>don't have any account? Register <Link href="/auth/register" className='font-bold hover:underline text-blue-600'>HERE</Link></span>
                </div>
            </div>
        </section>

        </>
    )
}
