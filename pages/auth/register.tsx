import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { AuthLayout } from '@/layouts/AuthLayout'

Register.layout = AuthLayout

export default function Register() {
    return (
        <>

        <Head>
            <title>Register</title>
        </Head>

        <section id='register-page' className='flex items-center justify-center min-h-screen'>
            <div className="card bg-base-200 shadow-xl w-96">
                <div className="card-body">
                    <h2 className="card-title mx-auto mb-4">Register</h2>

                    <div className="mb-4">
                        <label className="form-control">
                            <div className="label">
                                <span className="label-text">Email</span>
                            </div>

                            <input type="email" placeholder="Type here" className="input input-bordered" name="email" />
                        </label>
                    </div>

                    <div className="mb-4">
                        <label className="form-control">
                            <div className="label">
                                <span className="label-text">Username</span>
                            </div>

                            <input type="text" placeholder="Type here" className="input input-bordered" name="username" />
                        </label>
                    </div>

                    <div className="mb-4">
                        <label className="form-control">
                            <div className="label">
                                <span className="label-text">Password</span>
                            </div>

                            <input type="password" placeholder="Type here" className="input input-bordered" name="password" />
                        </label>
                    </div>

                    <div className="mb-4">
                        <label className="form-control">
                            <div className="label">
                                <span className="label-text">Confirm Password</span>
                            </div>

                            <input type="password" placeholder="Type here" className="input input-bordered" name="confirm_password" />
                        </label>
                    </div>

                    <div className="card-actions mb-4">
                        <button className="btn btn-success w-full" type='submit'>Register</button>
                        <Link href="/" className="btn btn-error w-full">Back</Link>
                    </div>

                    <span>have any account? Login <Link href="/auth/login" className='font-bold hover:underline text-blue-600'>HERE</Link></span>
                </div>
            </div>
        </section>

        </>
    )
}
