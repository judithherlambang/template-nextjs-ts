import React from 'react'
import Head from 'next/head'
import { MainLayout } from '@/layouts/MainLayout'

Home.layout = MainLayout

export default function Home() {
  return (
    <>
    
    <Head>
        <title>Home</title>
    </Head>

    <section id='home-page' className='container'>
      <h1 className='text-5xl my-12'>Ahtaum Template NextJS</h1>

      <div className='flex lg:flex-row flex-col justify-around gap-8 my-8'>
        <div className="hero min-h-96 bg-base-100 hover:bg-base-200 shadow-xl">
          <div className="hero-content text-center">
            <div className="max-w-md">
              <h1 className="text-5xl font-bold">This is My Template</h1>
              <p className="py-6">Provident cupiditate voluptatem et in. Quaerat fugiat ut assumenda excepturi exercitationem quasi. In deleniti eaque aut repudiandae et a id nisi.</p>
              <button className="btn btn-primary">Get Started</button>
            </div>
          </div>
        </div>

        <div className="card w-96 bg-base-100 hover:bg-base-200 shadow-xl">
          <div className="card-body">
            <h2 className="card-title">Card title!</h2>
            <p>If a dog chews shoes whose shoes does he choose?</p>
            <div className="card-actions justify-end">
              <button className="btn btn-primary">Buy Now</button>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    </>
  )
}
